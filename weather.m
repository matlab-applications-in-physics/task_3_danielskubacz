%MATLAB 2020b
%Name: weather
%Author: Daniel Skubacz
%Date: 14.12.2020

%saving data from websites:
station1URL = 'https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json';
station2URL = 'https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json';
station3URL = 'https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json';
station1URL_Data = webread(station1URL);
station2URL_Data = webread(station2URL);
station3URL_Data = webread(station3URL);

station1_WTTR = 'http://wttr.in/katowice?format=j1';
station2_WTTR = 'http://wttr.in/raciborz?format=j1';
station3_WTTR = 'http://wttr.in/bielsko-biala?format=j1';
station1_WTTR_Data = webread(station1_WTTR);
station2_WTTR_Data = webread(station2_WTTR);
station3_WTTR_Data = webread(station3_WTTR);

%reading the sunrise and sunset time:
Sunrise_1 = station1_WTTR_Data.weather(1).astronomy.sunrise;
Sunset_1 = station1_WTTR_Data.weather(1).astronomy.sunset;
Sunrise_2 = station2_WTTR_Data.weather(1).astronomy.sunrise;
Sunset_2 = station2_WTTR_Data.weather(1).astronomy.sunset;
Sunrise_3 = station3_WTTR_Data.weather(1).astronomy.sunrise;
Sunset_3 = station3_WTTR_Data.weather(1).astronomy.sunset;

%counting the WCT(Wind Chill Temperature) with the formula form
%"https://math.info/Misc/Wind_Chill/", which works only above 40℉ (4,4(℃):
%Formula: WCT(℉) = 35,74 + 0,6215*T - 35,75*(v^0,16) + 0,4275*T*(v^0,16)
%Where: T - Air Temperature (℉); v - Wind Speed (mph)

WCT1_WTTR = 35.74 + 0.6215.*(station1_WTTR_Data.current_condition.temp_F) - 35.75.*((station1_WTTR_Data.current_condition.windspeedMiles).^(0.16)) + 0.4275.*(station1_WTTR_Data.current_condition.temp_F).*((station1_WTTR_Data.current_condition.windspeedMiles).^(0.16)); %(℉)
WCT2_WTTR = 35.74 + 0.6215.*(station2_WTTR_Data.current_condition.temp_F) - 35.75.*((station2_WTTR_Data.current_condition.windspeedMiles).^(0.16)) + 0.4275.*(station2_WTTR_Data.current_condition.temp_F).*((station2_WTTR_Data.current_condition.windspeedMiles).^(0.16)); %(℉)
WCT3_WTTR = 35.74 + 0.6215.*(station3_WTTR_Data.current_condition.temp_F) - 35.75.*((station3_WTTR_Data.current_condition.windspeedMiles).^(0.16)) + 0.4275.*(station3_WTTR_Data.current_condition.temp_F).*((station3_WTTR_Data.current_condition.windspeedMiles).^(0.16)); %(℉)
%Conversion formula:
%1℃ = (5/9)*(1℉ - 32)

WCT_1_WTTR = (5/9)*(WCT1_WTTR - 32); %(℃)
WCT_2_WTTR = (5/9)*(WCT2_WTTR - 32); %(℃)
WCT_3_WTTR = (5/9)*(WCT3_WTTR - 32); %(℃)

%Reading and writing weather data for every of three locations:
Weather_Data_IMGW_1 = [ str2num(station1URL_Data.godzina_pomiaru) str2num(station1URL_Data.temperatura) str2num(station1URL_Data.predkosc_wiatru) str2num(station1URL_Data.wilgotnosc_wzgledna) ];
Weather_Data_IMGW_2 = [ str2num(station2URL_Data.godzina_pomiaru) str2num(station2URL_Data.temperatura) str2num(station2URL_Data.predkosc_wiatru) str2num(station2URL_Data.wilgotnosc_wzgledna) ];
Weather_Data_IMGW_3 = [ str2num(station3URL_Data.godzina_pomiaru) str2num(station3URL_Data.temperatura) str2num(station3URL_Data.predkosc_wiatru) str2num(station3URL_Data.wilgotnosc_wzgledna) ];
Weather_Data_WTTR_1 = [ str2num(station1_WTTR_Data.current_condition.localObsDateTime) str2num(station1_WTTR_Data.current_condition.temp_C) str2num(station1_WTTR_Data.current_condition.windspeedKmph) str2num(station1_WTTR_Data.current_condition.humidity) str2num(station1_WTTR_Data.current_condition.pressure) WCT_1_WTTR ];
Weather_Data_WTTR_2 = [ str2num(station2_WTTR_Data.current_condition.localObsDateTime) str2num(station2_WTTR_Data.current_condition.temp_C) str2num(station2_WTTR_Data.current_condition.windspeedKmph) str2num(station2_WTTR_Data.current_condition.humidity) str2num(station2_WTTR_Data.current_condition.pressure) WCT_2_WTTR ];
Weather_Data_WTTR_3 = [ str2num(station3_WTTR_Data.current_condition.localObsDateTime) str2num(station3_WTTR_Data.current_condition.temp_C) str2num(station3_WTTR_Data.current_condition.windspeedKmph) str2num(station3_WTTR_Data.current_condition.humidity) str2num(station3_WTTR_Data.current_condition.pressure) WCT_3_WTTR ];

Weather_conditions_table = table(Weather_Data_IMGW_1, Weather_Data_IMGW_2, Weather_Data_IMGW_3, Weather_Data_WTTR_1, Weather_Data_WTTR_2, Weather_Data_WTTR_3)
writetable(Weather_conditions_table, 'Weather_conditions_data.csv')


